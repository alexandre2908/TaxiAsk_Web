var mainApp = {};

var deletedata = function (uid) {
    firebase.database().ref("DriversInformation/" + uid).remove();
    window.location.replace("Gestion_Chauffeur.html");
};
var ajouter_chauffeur = function () {
    var databaseref = firebase.database().ref("DriversInformation");
    var nomcomplet = document.getElementById("nom_complet").value;
    var email = document.getElementById("email_chauf").value;
    var descritpion = document.getElementById("dexcription").value;
    var phone = document.getElementById("phone_number").value;
    var password = document.getElementById("password_chauf").value;

    firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(function () {
            var chauffeur = {
                "name": nomcomplet,
                "email" : email,
                "phone" : phone,
                "description_vehicule" : descritpion,
                "password" : password
            };
            databaseref.push(chauffeur);

            $('#modalRegisterForm').modal('hide');
            window.location.replace("Gestion_Chauffeur.html");

    }).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        if (errorCode === "auth/email-already-in-use"){
            alert("Cette Adresse email est deja enregistree")
        }else {
            alert(errorMessage);
        }
        // ...
    });
};
(function () {
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            // User is signed in.
            uid = user.uid;
            document.getElementById("userEmail").innerText = user.email;
        }else {
            uid = null;
            window.location.replace("Index.html");
        }
    });
    function logOut() {
        firebase.auth().signOut();
    }

    mainApp.logOut = logOut;
})();

(function () {
    var main_container = document.getElementById("main_container");
    var  firebasechauff = firebase.database().ref("DriversInformation");

    firebasechauff.once('value', function(snapshot) {
        snapshot.forEach(function(childSnapshot) {
            var childKey = childSnapshot.key;
            var childData = childSnapshot.val();
            var headercard = " <div class=\"col-3 mt-5 ml-3\">\n" +
                "                <div class=\"card testimonial-card\">\n" +
                "\n" +
                "                    <!--Bacground color-->\n" +
                "                    <div class=\"card-up indigo lighten-1\">\n" +
                "                    </div>\n" +
                "                    <!--Avatar-->\n" +
                "                    <div class=\"avatar\"><img src=\"css/fpo_avatar.png\" class=\"rounded-circle\">\n" +
                "                    </div>\n" +
                "\n" +
                "                    <div class=\"card-body\">"
            var chauffeur_name = "<h4 class=\"card-title\" id=\"test_email\">";
            chauffeur_name += childData.name;
            chauffeur_name += "</h4> <hr>";

            var  chauffeur_info = "<p><i class=\"fa fa-envelope-open\"></i>";
            chauffeur_info += childData.email +" </br> "+ childData.phone;
            chauffeur_info += "</p> ";

            var  footercard = "</div>\n" +
                "<div class=\'card-footer\'> <button class='btn btn-danger ml-4' onclick='deletedata(\"";
            footercard += childKey +"\");'>Supprimer</button></div>"+
                "                </div>\n" +
                "        </div> "
            main_container.innerHTML += headercard+chauffeur_name+chauffeur_info+footercard;
        });
    });
})();
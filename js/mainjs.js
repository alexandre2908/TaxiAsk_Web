var mainApp = {};

(function () {
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            // User is signed in.
            uid = user.uid;
            document.getElementById("userEmail").innerText = user.email;
        }else {
            uid = null;
            window.location.replace("Index.html");
        }
    });
    function logOut() {
         firebase.auth().signOut();
    }

    mainApp.logOut = logOut;
})();



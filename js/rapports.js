var mainApp = {};

(function () {
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            // User is signed in.
            uid = user.uid;
            document.getElementById("userEmail").innerText = user.email;
        }else {
            uid = null;
            window.location.replace("Index.html");
        }
    });
    function logOut() {
        firebase.auth().signOut();
    }

    mainApp.logOut = logOut;
})();

(function () {
    var nombre_commande = document.getElementById("nombre_commande");
    var  firebasechauff = firebase.database().ref("rapports");

    firebasechauff.once('value', function(snapshot) {
       nombre_commande.innerText= ""+snapshot.numChildren();
    });
})();
(function () {
    var nombre_chaufeurs = document.getElementById("nombre_chaufeurs");
    var  firebasechauffeurs = firebase.database().ref("DriversInformation");

    firebasechauffeurs.once('value', function(snapshot) {
        nombre_chaufeurs.innerText= ""+snapshot.numChildren();
    });
})();
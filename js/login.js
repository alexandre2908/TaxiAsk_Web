var login = function () {
    var email = document.getElementById("txt_email").value;
    var password = document.getElementById("txt_password").value;

    firebase.auth().signInWithEmailAndPassword(email, password).
        then(function (firebaseuser) {
        window.location.replace("Dashboard.html");
    }).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // ...
        if (errorCode ==="auth/user-not-found"){
            alert("Adresse email ou Mots de passe incorrect");
        }else if (errorCode === "auth/wrong-password"){
            alert("Mots de passe incorrect");
        }
        alert(errorCode);
    });
};